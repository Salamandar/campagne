#!/usr/bin/python
# -*- coding: utf-8 -*-

import datetime
import locale

locale.setlocale(locale.LC_ALL, 'fr_FR.UTF-8')

date_debut = datetime.date(2019, 7, 27)
nb_jours = 8
largeur_jour = 4
hauteur_minute = 0.015 * 1.475
heure_debut_journee = 9
heure_fin_journee = 6
heure_fin_campagne = 18
types_activite = {
    'libre':
        {
            'couleur':'LightSkyBlue',
            'nom':'Temps libre'
        },
    'formation' :
        {
            'couleur':'GreenYellow',
            'nom':'Formation',
        },
    'conf' :
        {
            'couleur':'Coral',
            'nom':'Conférence'
        },
    'groupe':
        {
            'couleur':'Yellow',
            'nom':'Vie de groupe'
        },
    'repas':
        {
            'couleur':'Gold',
            'nom':'Repas'
        },
    'po':
        {
            'couleur':'LightGreen',
            'nom':'Grand public'
        },
    'menage':
        {
            'couleur':'OrangeRed',
            'nom':'Tâches ménagères',

        },
    'plasci':
        {
            'couleur':'Violet',
            'nom':'Planète Sciences'
        }
    }
