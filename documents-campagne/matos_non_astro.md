# BRICOLAGE

- Caisse à outils											1
- Fer à souder												2
- Module Dremmel											1
- Perceuse														1
- Pistocolle gros											3
- Pictocolle petit										3
- Scie à bois													2
- Scie à Onglet avec boîte						1
- Chignolle														2
- Vrille															6
- Etau d'établi												1
- Scie sauteuse												1
- Lame Scie sauteuse									1
- Scie à métaux												1
- Lame scie à méteaux									1
- Serre-joints gros										4
- Serre-joints petits									4
- Etain																2 rouleaux
- Fil de fer													2 rouleaux 2 tailles
- Colle pistocolle gros								3 kg
- Colle pistocolle petit							1 jeu

# Papeterie

- tampons planète sciences						1
- Cutter gros													3
- Cutter petit												3
- Plastifieuse A3							1
- Cornières														6
- Agrapheuse													2
- Agrapheuse murale										1
- Module papéterie										3
- Ciseaux grand												4
- Crépon rouge												2
- stylos															5 jeux 5 couleurs
- Etiquetteuse									2
- Gommes															20
- Crayons à papier										5 boîtes
- Crayons de couleur									5 boites de 10
- Compas															5
- Règles															10
- Equerre															10
- Tableau blanc												3
- Tableau Paperboard									3
- Feutres															1
- Feutres gros												1
- Feutres veleda											3 jeux RVBN

# Papeterie consommable

- Ramette A4													3
- Attaches parisiennes								1 boîte
- Punaises														1 boîte
- Agraphes petit											1000
- Agraphes murales										2 boîtes
- Epingles														1 boîte
- Trombones														1 boîte
- Patafix															2 tablettes
- Scotch transparent									1 boîte
- Gaffeur marron											2
- Gaffeur gris												2
- Scotch double face									2
- Scotch inscriptible									2
- Scotch électricien									6
- Papier paperboard										2
- Feuilles à plastifieuse  Divers jeux A3, A4, A5, etc.
- Recharges étiquetteuses
- Rubalise (rouge/blanche)						1
- Post-it															3x3 paquets 3 couleurs

# Outils pedago

- Modules ALU													tous
- Cartes du ciel											20
- Kit système solaire vivant					1
- Planétariums											2
- Malle ludothèque adulte							1
- Malle jeux ext adulte								1
- Plots colorés												1 jeu
- Télescope pédagogique								1
- Jeu de gabarits en carton et ficelle1

# Consommable pédago

- Peinture luminescente bleue					2
- Peinture luminescente rose					2
- Peinture luminescente jaune					2
- Peinture noire											2
- Peinture bleue											2
- Peinture jaune											2
- Peinture rouge											2
- Peinture blanche										2
- Pinceaux														2 jeux
- Ouatte															3
- Ficelle															3
- Gobelets														20
- Pâte à modeler											3 couleurs
- Spot hallogène											6
- Brumisateur													4
- Jeu pic à brochette x100						3
- Cure-dents													2
- Boules polystyrène					Jeu diverses tailles en grand nombre
- Cotillons								1 ou 2 sachets
- Cartes du ciel en kit (parcours PO)         10

# Matériaux

- Carton															10
- Aquilux															12
- Polystyrène													3 plaques
- Plaques mousse bulle blanche 40x40	prendre le carton (servira aussi GMAPS)
- Bois (planches)											3
- Bois (tiges)												3
- Métal (tiges filtées)								3
- Métal (Vé)													3

# Documentation

- Module biblio astro									Voir Zotero
- Maquette grandeur nature sys. sol.	1 C'est quoi ???
- DOcuments campagne									Change chaque année
- Pas à Pas Dans l'Univers	    	Il y'en a dans les modules ALU-OBS

# Cuisine et vie quot

- Appareil croque monsieur						1
- Mallette couteau et ustensile				1
- Presse agrume electrique						1
- Plats plastique											8
- Raclette														1
- Thermos															3
- Papiers toilettes	pour							700 jour/personne
- savonette														10
- Sac Poubelles												3 rouleaux
- Essuie-tout													4 rouleaux
- Torchons														4
- Malle convivialité									1
- Tissus noir													1
- Jerrican d'eau											2
- Saladiers inox											2
- Produits entretien DIVERS ET VARIES										1
- Film plastique alimentaire						2 rouleaux 2 tailles
- Aluminium															2
- Papier tue-mouche											10

# Informatique

- Vidéoproj														3
- Adaptateurs écran>projo							autant que possible
- Petite sono													1
- Module reseau												1 : contenu à détailler
- ecran projection										2
- Imprimante/scanneur couleur					0 on prend celle de l'écoloc, au moins ca marche
-  * scanner : je ne me souviens pas d'en avoir vu dans le matériel PlaSci ; mais l'imprimante de l'écoloc fait scanner aussi je crois ?
- Imprimante laser N&b								idem
- * n'est-il pas intéressant de prendre tout-de-même une imprimante LASER N&B ? pour ne pas se poser la question du remboursement à chaque essai d'impression
- Ordi fixe             							8
- Ordi portable         							8
- Clef USB avec logiciels astro à installer
- Portable pole astro									1

# Autres

- Caisses multiprises / enrouleurs astro 2
- Caisse monnaie											1
- Caisse pharmacie										3
- Lampe frontale rouge		le plus possible (cf présence de LSF) (voir malle ALU-OBS)
- gant phospho												le plus possible
- Ventilateur													2
- Bâche (de BONNE qualité)						10
- Lampe extérieure rouge							3
- Bannière Plasci											1
- Propagandes plasci (tracts)					1 petite caisse
- Gants de manutentions								10 ( = 30 €)
- Talkie-Walkie												1 boîte
- Autocollant plasci
- flechage soirée porte ouverte				1 (année précédente)
- Com' AFA nuit des étoiles						1 caisse
- Kakemono Plasci											1 jeu

