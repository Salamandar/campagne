#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
    Ce petit script permet de calculer le nombre de fois auquel une personne doit s'inscrire à une tâche.
    Il tient compte de la fréquence de la tâche, du coefficient accordé à la tâche (ex : le ménage final vaut 2 tâche simple) et du nombre de personne nécessaire pour réaliser chaque tâche
"""
taches = {
        'menage-final' : {
            'coef' : 2,
            'nb_gens' : 10,
            'nb' : 1
            },
        'vaisselle-midi' : {
            'coef' : 1,
            'nb_gens' : 6,
            'nb' : 6 # midi
            },
        'vaisselle-soir' : {
            'coef' : 2,
            'nb_gens' : 6,
            'nb' : 9
            },
        'mise-table' : {
            'coef' : 1,
            'nb_gens' : 4,
            'nb' : 8 #uniquement le soir, le midi c'est buffet, les gens vont chercher leur vaisselle
            },
        'poubelle' : {
            'coef' : 1,
            'nb_gens' : 2,
            'nb' : 6 # dimanche au vendredi
            },
        'services-repas-soir' : {
            'coef' : 1,
            'nb_gens' : 4,
            'nb' : 6 # dimanche au vendredi
        }
    }

nb_participants = 61 # ne pas tenir compte des dispensés

total_inscriptions = 0 # le nombre total de nom à être inscrit, toute tache confondu

for description in taches.values():
    inscription_tache = description['nb'] * description['nb_gens'] * description['coef'] # Le nombre d'inscription total pour cette tache
    total_inscriptions = total_inscriptions + inscription_tache

print ("Il faut que chaque personne s'inscrive " + str (total_inscriptions/nb_participants) + "fois")
