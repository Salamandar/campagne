# Soirée Portes ouvertes d'astronomie à Barret-sur-Méouge

Madame, monsieur,

Je vous écris pour vous annoncer que dans le cadre d'une semaine de formation à l'Astronomie, l'association d'éducation populaire *Planète Sciences* organise une soirée de découverte de l'astronomie ouverte au public.

Cette soirée aura lieu le vendredi 2 Août 2019 à partir de 20h30, au gîte "L'Écoloc", à Barret-sur-Méouge, 05300, Hautes-Alpes.


Nous y proposerons :
- une découverte du ciel
- des observations au télescope
- des séances planétarium
- des animations pour enfants
- une conférence de Clément Perrot, chercheur au Very Large Telescope (Chili)

Merci d'avance de diffuser cette information.
Vous pourrez trouver en pièce jointe l'affiche de l'événement.
Vous pourrez trouver l'affiche de l'événement ici : https://salamandar.fr/jirafeau/f.php?h=3WodJe4w&p=1.

Je vous prie d'agréer, Madame, Monsieur, l'expression de mes salutations distingués.

Pour Planète Sciences Astronomie, Félix Piédallu
