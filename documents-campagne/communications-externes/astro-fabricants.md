# Fabricants et revendeurs de matos astro

Vérification au 19 avril par Florian Targa.
Depuis http://www.astronomie-magasins.com/


info@axisinstruments.com,
contact@alpao.com,
dobson.2005@gmail.com,
contact@imagine-optic.com,
franck.griere@orange.fr,
info@pierro-astro.com,
skyvisionstore@hotmail.com,
sales@solarscope.com,
sud.dobson@gmail.com,
valmeca@club-internet.fr,
info@airylab.com,
info@atelierdumoulindeblande.fr,
contact@laclefdesetoiles.com,
info@maison-astronomie.com,
info@bresser.fr,
info@meteotronic.com,
ovision@aol.com,
info@pierro-astro.com,
contact@promo-optique.com,
info@optique-unterlinden.com,
info@foto-zumstein.ch,
ne@photovision.ch,
ls@photovision.ch,
info@lo.be,
info@slotte.be,
service@astro-shop.fr,
astronome@wanadoo.fr,
ovision@aol.com

https://www.uranie-astronomie.fr/contact/
https://www.telescopes-et-accessoires.fr/fr/nous-contacter
http://www.paralux.fr/contact.php
https://www.natureetdecouvertes.com/wecare-form
https://www.naturoptic.com/contact.php?formulaire=1
https://www.medas-instruments.com/nous-contacter
https://www.m42optic.fr/pro/catalog/contact_us.php
https://www.schott.com/france/french/contact/mail_form.html?mail_model_type=contact&mail_model_id=965045895&referer=%2Ffrance%2Ffrench%2Findex.html
https://www.shelyak.com/contact/
http://skymeca.com/contact-form.php
https://www.legrand-optique.fr/nous-contacter.php

## Mail type (à adapter au cas par cas)


Madame, Monsieur,

Notre association Planète Sciences œuvre depuis 1962 à la médiation scientifique, et est particulièrement active dans le domaine de l'astronomie.

Nous avons trouvé votre nom dans l'annuaire des vendeurs et fabricants de matériel astronomique, et c'est en tant qu'acteur du monde de l'astronomie que nous vous contactons aujourd'hui. **Nous souhaitons en effet annoncer un événement associatif fort**, susceptible d'intéresser vos salarié·e·s et client·e·s, qui sont également, à n'en pas douter, amateurs·trice·s ou curieux·seuses d'astronomie.

Association d'éducation populaire aux sciences et techniques, Planète Sciences organise sa traditionnelle Campagne Astronomie du 27 juillet au 4 août à Barret-sur-Méouge, dans les Hautes-Alpes. **Une semaine de découverte et d'échanges pour pratiquer l'astronomie, réunissant une soixantaine de personnes encadrées par des bénévoles passionné·e·s et compétent·e·s.**

Nous proposons **six parcours de formations, permettant à chacun de progresser à son rythme**, du débutant ou de la débutante souhaitant apprendre à utiliser son premier télescope à l'amateur ou amatrice confirmé·e, qui souhaite s'initier ou se perfectionner en astrophotographie. Chaque formation alterne temps théoriques en après-midi et temps d'observations en soirée.

Trois parcours orientés vers l'observation et validés par l'AFA, pour se faire plaisir, se former et vivre à un rythme totalement décalé :

🌜 Première étoile — Initiation théorique et pratique
🌜 Deuxième étoile — Approfondissement théorique et pratique
🌜 Troisième étoile — Astrophotographie et imagerie numérique

Nous proposons aussi **trois parcours de formation à l'animation**, pour apprendre à transmettre le plaisir de l'astronomie avec des outils spécifiques :

🌜 Agrément pédagogique Petite Ourse — Animer avec des 8-14 ans (validation AFA)
🌜 Agrément pédagogique Arpenter l’Univers — Animer en collège-lycée (parcours spécifique à Planète Sciences)
🌜 Agrément Planétarium Numérique (parcours spécifique à Planète Sciences)


*Chacun de ces parcours se décline sous deux modalités : base, donnant une formation complète aboutissant à une certification, et consolidation, pour réviser et approfondir en autonomie ses compétences, en étant accompagné·e par les formateurs et formatrices et en disposant des ressources matérielles et pédagogiques de l'association.*

Seront bien sûr aussi au rendez-vous, comme chaque année, des temps de convivialité et d'échanges, autour de l'astronomie, mais pas uniquement ! Bonne humeur garantie.

Vous trouverez plus de renseignements sur notre site internet : http://www.planete-sciences.org/astro/campagne

Nous joignons à ce message une affiche annonçant la campagne, qu'il vous est possible - nous vous en serions très reconnaissants - d'afficher dans vos locaux ou magasins. Le cas échéant, nous disposons d'un fichier de plus grande résolution, n'hésitez pas à nous le demander.

En vous remerciant chaleureusement, astronomiquement vôtre,

Maïeul Rouquette, pour l'équipe d'organisation de la Campagne Astro 2019


