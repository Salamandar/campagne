
Bonjour ...

Nous sommes à moins de 20 jours du début de la Campagne Astronomie de Planète Sciences. C'est LA semaine de formation à l'astronomie et surtout à la pédagogie de l'astronomie pour notre association. Elle constitue le moment idéal dans l'année, voire le seul, pour se former de manière approfondie sur cette thématique et être apte à animer des ateliers à partir de la rentrée. Puisqu'il nous reste de la place sur nos parcours pédagogiques, nous nous permettons de te contacter directement pour te proposer de nous rejoindre.

--------------------------- Profil ALU
Étant donné tes connaissances déjà acquises en astronomie, nous pensons qu'une formation Arpenter l'Univers pourrait t'être utile. Celle-ci permet d'être apte à sensibiliser à l'astronomie voire de mener des projets expérimentaux en collège-lycée. Durant toute l'année, Planète-Sciences a besoin d'animateur·trice·s compétent·e·s pour mener ces opérations.

Plus d'infos sur Arpenter l'Univers : https://www.planete-sciences.org/astro/arpenterlunivers/presentation

Néanmoins, il existe aussi un parcours délivrant un agrément Petite Ourse et un autre permettant de devenir technicien.ne et animateur.trice de Planétarium Numérique, si cela te parle plus.

--------------------------- Profil Plané
Nous pensons qu'une formation pour devenir technicien·ne et animateur·trice de Planétarium Numérique pourrait t'intéresser. Durant toute l'année, nous avons besoins de personnes compétent·e·s pour organiser des séances avec cet outil très demandé.

Néanmoins, il existe aussi un parcours Arpenter L'Univers et un autre délivrant un agrément Petite Ourse, si cela te parle plus.

----------------------------- Profil ourse
Nous pensons qu'une formation Petite Ourse pourrait t'intéresser. Cet agrément pédagogique permet d'encadrer des jeunes de 8 à 14 ans débutants en astronomie sur des stages et mini-séjours. En outre, cela te permettra d'acquérir un socle de compétences et de savoirs en astronomie. C'est pourquoi cette formation ne nécessite pas de connaissance préalable en la matière.

Plus d'infos sur la Petite Ourse : https://www.afastronomie.fr/la-petite-ourse

Néanmoins, il existe aussi un parcours Arpenter L'Univers et un autre permettant de devenir technicien.ne et animateur.trice de Planétarium Numérique, si cela te parle plus.

--------------------------- Reprise du mail commun
Nous essayons de te proposer un tarif accessible qui comprend la formation, l'hébergement et la restauration pour la semaine complète. Seul le transport est à part, et nous essayons tant que possible de mettre en place du covoiturage. Il est possible de payer en plusieurs fois et en chèques vacances, mais aussi de faire financer toute ou partie de la formation par un organisme tiers ou une délégation régionale de notre réseau.

D'autres arrangements pourraient certainement être imaginés. Nous ne pensons pas à tout donc n'hésite pas à nous faire part de tes idées. Dans tous les cas, la contribution souhaitée est de 295 €. Si ce n'est pas possible mais qu'une formation t'intéresse, dis-le nous que l'on puisse échanger plus spécifiquement sur ton cas. Si c'est quelque chose que tu peux te permettre, merci de nous indiquer également si tu peux aller plus loin en faisant par exemple un don défiscalisé à l'association qui nous aiderait à équilibrer la Campagne Astro.

Nous restons à ta disposition pour toutes questions liées à la Campagne Astronomie, n'hésite surtout pas à nous contacter.

Si tu souhaites t'inscrire, merci d'utiliser le formulaire en ligne
 https://www.planete-sciences.org/astro/campagne-astronomie/inscription

En espérant te voir prochainement,

Maïeul ROUQUETTE, coordinateur bénévole de la campagne
maieul@maieul.net
06 74 53 04 23

Quentin VIEL, bénévole et formateur sur la campagne
quentin.viel@planete-sciences.org
06 85 45 14 41

Jean-Baptiste BELLIER, responsable salarié du Pôle Astronomie
astronomie@planete-sciences.org
01 69 02 76 10



