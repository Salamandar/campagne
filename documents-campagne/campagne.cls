\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{campagne}[02/07/2019 v2.7]
\def\taillepolice{12pt}

\newif\ifplaquette@%
\newif\iflivret@
\newif\iffondperdu@
\newif\ifnumeropage@

\RequirePackage{xkeyval}
\DeclareOptionX{plaquette}{\plaquette@true}
\DeclareOptionX{livret}{
  \livret@true
  \def\taillepolice{11pt}
}
\DeclareOptionX{fondperdu}{\fondperdu@true}
\DeclareOptionX{numeropage}{\numeropage@true}
\ProcessOptionsX*

\LoadClass[twoside,\taillepolice,a4paper]{article}
\RequirePackage{microtype}
\RequirePackage{nowidow}
\RequirePackage{csquotes}
\RequirePackage[margin=2cm,a4paper,bottom=1cm]{geometry}
\RequirePackage{polyglossia}
\setmainlanguage{french}
\RequirePackage{libertine}
\parindent=0pt
\newfontfamily\emojii{Noto Emoji}%http://www.fontspace.com/google/noto-emoji
\RequirePackage{graphicx}
\graphicspath{{images/}{../images/}{../../images/}}
\RequirePackage{fancyhdr}
\pagestyle{fancy}
\RequirePackage[framemethod=TikZ]{mdframed}
\RequirePackage[hidelinks ]{hyperref}
\renewcommand{\headrulewidth}{0pt}
\fancyhead{}
\RequirePackage{tikzlings-owls,tikzducks}
\newcommand{\marmottepage}{%
    \begin{tikzpicture}[scale=0.5]
        \owl
        \duck[xshift=-12.5,yshift=5, invisible,signpost=\scalebox{0.6}{\thepage}]
    \end{tikzpicture}
}
\ifplaquette@\else
 \geometry{%
   bottom=2cm,
   footskip=2cm
 }
\fi

\ifnumeropage@
  \fancyhead[LE,RO]{%
     \marmottepage
  }%
\fi
\iflivret@
  \fancyhead[LE,RO]{%
     \marmottepage
  }%
  \setbox0=\hbox{\marmottepage}
  \geometry{
      hmargin=2.5cm,
      left=1.5cm,
      right=1.5cm,
      layout=a5paper,
      paper=a5paper,
      head=\ht0,
      headsep=0pt,
    }
  \iffondperdu@
    \geometry{
      layoutoffset=3mm,
      papersize={154mm, 216mm}
    }
  \fi
    \setlength\emergencystretch{\hsize}
    \fancyfootoffset{1.5cm}
\else
	\fancyfootoffset{2cm}
\fi
\fancyfoot{}
\fancyfoot[L]{%
\includegraphics[width=\dimexpr\Gm@layoutwidth+1pt\relax]{barette-plaquette}}
\fancypagestyle{plain}{%
  \fancyhead{}%
}
\newenvironment{cadre}{
\begin{mdframed}[%
   middlelinecolor=red,
   middlelinewidth=2pt,
   roundcorner=10pt,
   innertopmargin=2.5ex,
   innerbottommargin=2.5ex,
   ]
    \centering
}{\end{mdframed}}

\RequirePackage{setspace}
\onehalfspacing
\newcommand{\titre}[1]{%
  \bgroup\LARGE%
    {\emph{#1}}%
  \egroup\nopagebreak[4]%
}

\RequirePackage[]{enumitem}
\setlist[itemize]{%
  label={\bgroup\emojii🌜\egroup}
}
\newcommand{\subtitle}[1]{\gdef\@subtitle{#1}}

\renewcommand{\maketitle}{%
  \begin{center}%
  {\Huge \@title}%
  \vskip 1.5\baselineskip%
  {\Large \emph{\@subtitle}}%
  \thispagestyle{plain}
  \end{center}%
}

\gappto\captionsfrench{\renewcommand{\contentsname}{Sommaire}}
\setcounter{tocdepth}{2}

\renewcommand{\thesection}{}
\renewcommand{\thesubsection}{}
\renewcommand{\thesubsubsection}{}
\hypersetup{bookmarksdepth=paragraph}

\RequirePackage{xspace}
\def\datecampagne{2019\xspace}
\def\Plasci{Planète Sciences\xspace}
\def\SA{Pôle Astronomie\xspace}
\newif\ifcampagnecourt
\newcommand{\campagne}{%
  \ifcampagnecourt
    Campagne Astro%
  \else
    Campagne Nationale d'Astronomie%
  \fi\xspace
}

\newcommand{\campagnes}{%
  \ifcampagnecourt
    Campagnes Astro%
  \else
    Campagnes Nationales d'Astronomie%
  \fi\xspace
}
