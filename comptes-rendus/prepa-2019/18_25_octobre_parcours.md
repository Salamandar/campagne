# Restructuration des parcours

Concertation tripartite Maïeul / Quention / DSC

## But

- Proposer des niveaux intermédiaires
- Permettre une alternative entre pédagogie "vertical" et autonomie.

## Proposition

- Chaque parcours, observationnel ou pédagogique, se décline sous deux modalités (sauf pour le parcours libre):
	- **Base** : pédagogie plus guidée, du type de ce qui s'est fait en Etoile 1 en 2018
	- **Consolidation** : pédagogie plus autonome, plus démarche experimental, du type de ce qui s'est fait en Etoile 2 en 2018
- Concrètement, pour chaque parcours nous avons mis des exemples de ce qui peut se faire sur le site du pôle : https://huit.re/r60oDwf0
- Prérequis:
	- Pour les parcours péda, on ne peut s'inscrire en consolidation sans la base
	- Pour les parcours observationnels, c'est possible mais on recommande quand même la base, et à l'inscription une case à cocher est proposée pour que la personne dise qu'elle a bien la base.
- Seul les modalités base délivrent une qualification
- On peut s'inscrire en Etoile 2 sans avoir formellement l'Etoile 1, et en Etoile 2 sans avoir formellement l'Etoile 3 mais:
	- Ce n'est pas recommandé
	- L'attribution de la qualification Etoile n+1 est conditionné par la vérification du niveau Etoile n. Donc les responsables de l'Etoile 3 vérifie dans ce cas là que la personne peut bien avoir une Etoile 2 avant de lui donner l'Etoile 3.
- Si une personne pense avoir le niveau mais sans avoir la qualification, elle s'inscrit en base si elle veut obtenir la qualification (on lui propose une case à cocher spéciale lors de l'inscription).
- En terme d'organisation de binomes de formateur-trices, vous êtes libres, mais il peut être bon d'avoir 1 personne plus chargée de la base et une autre plus chargée de la consolidation. En règle générale, les 2 sont avec le gens en base, mais la responsable "consolidation" peut se détacher à tout moment pour aider les gens en consolidation.
- Dans tous les cas, pas plus de 10 personnes en base + consolidation.
- Une personne qui s'est inscrite en consolidation peut suivre les formations base si elle juge cela nécessaire.
- On s'assure (le groupe coordination), après discussion avec les responsables de formation, que les parcours Consolidation disposent d'une bibliothèque, d'une ressourcothèque et de matériel solide pour être autonome.

##  Besoin préparatoire en amont

Pour chaque parcours il faut

- En base, une pédagogie progressive et adaptée au niveau
- En consolidation, des impulses / pistes d'experiences ou d'action à mener (à la manière des mini-défis d'étoile 2).


## Application pour 2019

- Merci de valider ou non le principe de structuration sur
https://framadate.org/sEXDO5YIOTMXPQNh
- Retours attendus d'ici 4 novembre
- Pour 2019, selon le nombres de formateurs/trices qu'on a, les 2 modalités pourraient ne pas être ouvertes selon les parcours. JB et Maïeul nous chargeons, en coordination avec les responsables de parcours, d'orienter les gens.
