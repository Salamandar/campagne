Présent·e·s : Maïeul, Julien, Noëlie, Grégoire, Lydie
# Répartition des postes

- Julien Jacquemmoz : responsable informatique
	- déterminer en amont le matériel nécessaire
	- sur place, mettre en place le réseau, la salle info et tutti quanti
- Grégoire : responsable vie quotidienne
	- être présent aux temps de vaisselle collective
	- gérer la répartition des tâches (l'annoncer, mais aussi surveiller)
	- vérifier qu'il y a pecu/savonette/poubelle partout
- Noëlie
	- Gérer les météos
	- Voir avec l'équipe cuisine les animations de soirée
	- Animer le jeu de présentation
- Lydie
	- Accueillir les gens au télèphone pour guider lors de la venue
	- présenter les lieux si viennet pour la première fois
	- battre le rappel sur le champ astro lors des jingles (cf plus bas)
- Maïeul :
	- gérer les passages de paroles
	- gérer le lancement des jingle
	- gérer les relations avec l'écoloc


# Gestion des retards

- Utiliser un jingle. Lydie cherche le jingle.
- Comme le jingle ne peut pas être hyper fort, il faut aller chercher les gens qui seraient du côté du champ astro > Lydie
- On metterait un jingle à ces temps
	- Vie quotidienne
	- Apero
	- Repas
	- Reprise d'activité le soir

# Décision pour la gestion des taches

Problème :
	- il faut que les taches soit décidées ensembles
	- mais là forme actuelle fait que les gens ne s'investissent pas dans la décision, et finalement ce sont les mêmes qui font tout

Solution adoptée pour cette année:
	- Dans le livret on met une base de départ
	- Le premier jour, on met un grand panneau où les gens peuvent soumettrre des propositions d'amendement ; des amendemnts peuvent être posés en avance par correspondance
	- Au repas du samedi soir, on annonce les amendements
	- jusqu'au dimanche apero, les gens peuvent voter par gommette pour les amendements proposition alternatives
	- annonce des décisions durant le repas du soir

Cela concernerait toutes les taches vies quotidiennes.

1. mise de table
        1. repas du soir
        2. petit dej astro
        3. petit dej matin
2. vaisselle :
        1. Matin
        2. Midi
        3. Soir
        4. Petit dej astro
3.  poubelles et tri selectifs
4.  nettoyage et rangement "régulier" du ref
5.  service repas du soir
6. rangement de fin de sejour


On fera dans une autre réunion une liste de proposition.

# Vaisselle du soir

prévoir 1 heure. Décaler officiellement le début du petit dej astro pour revenir à ce qu'on avait avant (1h).

# Vaisselle de la nuit

on reste en mode chacun fait sa vaiselle, mais on met des affichettes pour rappeler aux gens de la faire.

# Dispense des taches vie quotidennes collectives (sauf ménage de mi sejour / final)

- formateurs/trices
- coordinateur
- responsable vie quotidienne
- responsables matos


# Gestion du rangement final le dernier jour au matin

Considerer cela comme une tâche vie quotidienne. Les gens qui s'engagent à aider ce jour sont dispensés des autres taches.

# Boissons

Il y a eu une demande pour des boissons légères.
Planète sciences pourrait prendre en charge, par ordre de possibilité budgétaires
- Sirops
- Café
- Lait (je me suis permis de rajouter cela)
- Oranges à jus si possible budgétairement

Par ailleur il faut mettre des affichettes pour dire ce que plasci prend en charge et ce qu'il ne prend pas en charge + mettre dans livret.

# Livret

Il faut que les membres du pole "pratique" m'envoie une courte bio.

# Planning

Basculer le planning en système standard, avec les horaires affichées à gauche.

# Parking et chambres

Lydie réflechie à des propositions pour gérer les chambres / le parking.

Reflechir à comment on gère des voitures pour du matos astro perso lourd sur le champ astro.

