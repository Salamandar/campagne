Présents : Maïeul, JB, Quentin


1. Sous
---

La demande de défraiement de l'équipe d'organisation (= prise en charge du séjour) a bien été prise en compte, pour l'année 2019.
Au mois de novembre 2018, Quentin et JB feront des propositions,qui seront soumises au bureau du secteur.
Suivant ce mode:
	1. Coordinateur bénévole : prise en charge
	2. 1 Responsable matos  : prise en charge
	3. Responsables formations : 2 par parcours : prise en charge
	4. Responsable vie quotidienne : pas de prise en charge, mais on essaie d'étendre un max l'équipe pour partager les responsabilités

Nous ne pouvons pas encore dire comment la prise en charge sera faite ni à quelle hauteur: ce sera des propositions de Quentin et JB.
Libre ensuite à chacun et chacune de s'engager pour 2019 dans ces conditions ou pas. Mais si vous nous envoyez vos conditions en amont, cela peut aider à projeter.

2. Parcours de formation
---

On a une réunion le 4 octobre à 10 heures pour les parcours de formation. Merci à chacun·e de m'envoyer en privé un mail concernant:

- les parcours de formations qu'ielles jugent utile de maintenir
	a. pour la campagne
	b. Pour le pôle
- Ce qu'ielle voit à améliorer dans la structure et le contenu des parcours de formation, y compris lorsqu'il ne s'agit pas du sien.

Ces avis seront consultatifs.

3. Lancement des inscriptions
---
On attend d'avoir le point sur les défraiements et parcours pour lancer les inscriptions, donc en décembre-janvier.

4. Dates de la campagne
---

JB voit ce qu'il est possible de faire avec l'équipe cuisine, en tenant compte des avis des responsables de formations exprimés lors de la réunion de septembre

5. Nombre d'inscrits max
---
Cette question n'a pas été tranchée. Plusieurs paramètres rentrent en ligne de compte:

- le défraiement des formateurs
- le nombre de parcours de formation
- les travaux à l'écoloc qui risquent de faire perdre de la place
