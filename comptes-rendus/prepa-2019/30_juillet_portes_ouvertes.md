
# Différentes équipes réparties le long du trajet

* Talkies walkies dispos pour communiquer

* Les gens s'inscrivent sur une activité et se coordonnent ensuite en petit groupe par activité


# Accueil

Distribution de :
* Cartes du ciel
* Communication Planète Sciences
* Flyers avec la liste des événements de Buthiers


* Faire un plan du lieu

* Boîte à dons

# Écran de projection de photos (Saturne)

* Projection mal adaptée à cause de la lumière autour
* Idée : remplacer par un écran d'ordinateur


# Conférence

Clément perrot sur l'astronomie professionnelle.

* À quelle heure ?

* Une heure max ?



# Champ astro

* Petite ourse : jeux (distances système solaire) de l'accueil au champ astro (chemin avec Milestone)


Il va falloir dégager les voitures devant le champ astro.

Il faut mettre en place un télescope plus bas pour les enfants.

On peut mettre en place un "chemin" avec une complexité montante des informations.


* Petite ourse : chasse aux constellations

* Parcours d'observation du ciel

* En fin de soirée, lorsqu'il n'y a plus que le public très intéressé, on peut leur
  apprendre à pointer des objets

* Faire des groupes mixtes (étoile 1,2,3 ensemble)

* Mettre plusieurs bâches


# Planétarium

* où ? Devant la salle matos ? À l'intérieur?

* Il y aura 3 séances de planétarium
  * une séance par heure
  * 30 min de contenu
  * 45 min de séance

* Un thème différent par séance

-> Il faut préparer un système de "tickets" pour que le public choisisse sa séance.

* Il faut une personne en permanence devant la salle matos pour accueillir le monde
  et surveiller que les gens n'y rentrent pas.
