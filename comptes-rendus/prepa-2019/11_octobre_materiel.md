Présent : Julien Jacquemmoz, Sébastien (par intermmitence), Frédéric, Maïeul

# Anticipations des besoins

- Julien  va construire un formulaire pour que les responsables de parcours expriment clairement leur besoins en matériel informatique
- Sébastien et Frédéric feront de même pour le matériel astro

# Anticipation du matériel apporté par les participant·e·s

Le formulaire d'inscription comprendra une partie "matériel apportée" quelque soit le parcours. Julien et Sébastien vont m'aider à la construire.

# Semaine de prépa du matériel

Il faut 1 semaine à 2 personnes connaissant bien le matos pour bien le préparer.
Action à court terme:

- Demander les dispo des gens lors du formulaire d'inscription comme cette année car les petites mains sont aussi utiles.
- Sébastien : chercher une aide astromaker.



# Gestion du matériel au jour le jour

- En Bureau, discuté d'une forrmation au matériel pour les responsables de parcours, en amont.
- Plutôt que faire une réunion chaque soir pour discuter les besoins matos, on mettra une fiche de demande matériel par jour dans le bureau. Cette fiche devra être rempli par les responsables de formation avant 14h15 pour la formation du soir. Avantages:
	- moins de réunion
	- fiche formalisé
	- Sébastien aura l'info dès 14h15 et pourra préparer / vérifier le matériel au jours le jour.
- Contenu de la fiche : un tableau avec les colonnes suivantes :
	- Newton/Schmidt-Cassegrain/Dobson/Lunettes jumelles
	- Photo
	- Monture
	- Moteur
	- Diamètre
	- Parcours

# Règles d'utilisation

Préparer une affiche A3 résumant les régles d'utilisation, à placarder dans la salle astro
