# Synthèse des discussions diverses + vote

## But

- densifier la campagne
- faciliter le temps d'installation
- s'assurer que tout le monde soit là (hors parcours libre) le dimanche


## Organisation
- Jeudi précédent la campagne : chargement camions
- Vendredi soir arrivée de :
	- Camions
	- Coordinateur bénévole
	- Equipe cuisine
	- Responsable de l'accueil
	- Autres bénévoles
- Samedi
	-	déchargement camion
	- accueil + installation des participants **pas d'installation le dimanche sauf exception**
	- à midi : repas simple pour les gens arrivée le vendredi soir. Pris en charge par plasci, mais pas par l'équipe cuisine (des pâtes!)
	- samedi soir : premier repas pris en charge
	- samedi nuit : observation libre (les responsables de parcours ne sont pas tenus de faire quoi que ce soit)
- dimanche :
	- avant 10h : arrivée des derniers retardataires (train de nuit, gens qui habitent à côté)
	- 10h : lancement officiel de la campagne
	- 11h : premier temps d'atelier
	- 12h30 : repas buffet
	- 14h15 : temps vie quot
	- 14h30 : atelier
	- suite du dimanche : journée standard
- mercredi : journée libre
- vendredi soir : porte ouverte
- samedi :
	- 14h15 : début du rangement. Les responsables de formations peuvent prendre un temps bilan s'ils ne l'ont pas fait la veille.
	- 18h30 pendant la météo : retranmissions sous forme de stand pour parcours peda + obs
	- 19h30 : repas d'au revoir
	- 21h : rangement camion, nettoyage moitié du grand ref
- dimanche matin
	- rangement final, qui comptent comme "une tâche" dans le tableau des tâches ménagères
