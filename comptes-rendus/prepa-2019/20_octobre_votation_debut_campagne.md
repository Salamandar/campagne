# Résultat des votations du 20 octobre

## Présence lors du lancement de la campagne astro

8 participant·e·s

- La présence au lancement de la campagne astro est obligatoire :
	- 2 oui
	- 2 oui conditionnel
- La présence au lancement de la campagne astro est obligatoire sauf pour les parcours libres (veterans) :
	-	8 oui
- La présence au lancement de la campagne astro est faculative (les gens viennent quand ils souhaitent) :
	- 0 oui

Conclusion :

- la présence au lancement est obligatoire, sauf pour les parcours libres (veterans)
- Maïeul va adapter le formulaire d'inscription en conséquence

## Début de la campagne astro
7 participant·e·s

- Commencer la campagne à 14h le jour qui suit l'aménagement :
	- 3 oui
	- 1 non
	- 1 oui si
- Commencer la campagne à 10h le jour qui suit aménagement
	- 4 oui
	- 1 non
	- 2 oui si

Conclusion :

- la campagne commence à 10h le jour qui suit l'aménagement
- cela signifie que la plupart des gens doivent être  là dès le jour de l'aménagement
- par conséquent, plutot que de demander si les gens arrivent dès le samedi soir, boutons radios pour savoir si les gens arrivent le samedi soir ou le dimanche matin avant 10h
