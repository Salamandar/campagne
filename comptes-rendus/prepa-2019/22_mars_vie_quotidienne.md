# Réunion vie quotidienne
## Présents
Grégoire, Julien, Maïeul

## Proposition de règles amendables pour le bon fonctionnement de la vie quotidienne

- Système de poids pour que les tâches plus longues et pénibles soient mieux valorisées

    - la vaisselle du soir

    - le ménage de vraiment fin de séjour

- Document à part du livret pour ne pas l'encombrer et pouvoir l'amender plus facilement.


Ne pas oublier de mentionner les personnes dispensées de tâches
- formateurs·trices
- coordinateur
- responsable vie quotidienne
- responsables matos

groupe pour les tâches :
- menage de fin de séjour : 10 + responsables
- la vaisselle des repas : 6
- la mise de table : 4
- les poubelles et le tri des déchets : 2
- le service des repas du soir : 3

Un système automatique de calcul du nombre de tâches que doit effectuer une personne.

## Modalité concrète de débat avec les participant·e·s

La proposition d'amendement se ferait en deux étapes :
1. Environ un mois à l'avance, on propose le document qu'on aura fait par courriel aux participant·e·s

    - Possibilité de faire des amendements

    - Possible de poser des questions pour plus d'informations

2. Le premier jour de la campagne (samedi) :

        - on affiche le texte initial et les amendements déjà proposés

        - on peut en déposer d'autre expliquant qu'il est toujours possible d'en rajouter d'autres

3. Pendant le repas du  samedi soir,

     - on propose d'en discuter en petit groupe (genre par table ou section)

    - en mettant en libre service le doc et les amendement proposé

    - mais aussi du papier et stylo pour en écrire de nouveaux

4. Après le repas du samedi soir

    - on met tout les amendements sur un tableau suite au repas et on les ouvre aux votes.

    - Ce tableau comportera en plus des amendements trois colonnes : deux pour le vote (Oui/Non) et une troisième pour les commentaires et/ou explications.

    - Concrètement pour le vote, chacun pour ajouter un bâton dans la colonne de son choix pour chaque amendement.

5.

    - Le vote se clôt dimanche soir avant le repas du soir.

    - Pendant le repas les résultats seront annoncé et les incompatibilités potentielles réglées collectivement à ce moment


## Points non amendables
- La liste de tâches en soi (afin on peut faire des ajouts)
- La dispense des tâches (retirer ou ajouter des personnes de la liste)
- Le caractère équitable des tâches (tout le monde contribue pour qu'en moyenne tout le monde en ai fait autant, hors cas particulier (handicap, maladie, investissement dans l'orga,...)
- A mettre dans le livret "Si une personne pour des raisons de santés ou de handicap éprouve des difficultés é effectuer ces tâches, merci de venir en discuter avec l'équipe d'organisation"

Le cas échéant, un amendement pourra être refusé en amont par responsable vie quot + coordinateur, mais devra être correctement justifié.

## Questions pratiques & autres, sur le fonctionnement du rôle responsable vie quotidienne

- Tâches du responsable
    - veiller à la présence des élements indispensables au confort de base le premier jour : savon dans les salles de bain, papier toilette
    - organiser le débat sur les taches ménagère
    - petites réparations si besoin, avec notamment la clef du bazar où sont stockés les lampes etc
    - être présent à la vaisselle pour expliquer le fonctionnement
    - installer des poubelles dans les différents salles d'activité
    - voir si il y a besoin de papier tue-mouches
    - a compléter
## Documents à créer
- un panneau "petites annonces" (encart explicatif + post its et stylos) > Maïeul
-  vademecum vie quotidienne > Grégoire l'écrit
- document de proposition sur gestion taches quotidienne > Grégoire
