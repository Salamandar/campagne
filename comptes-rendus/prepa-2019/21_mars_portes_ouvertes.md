# Soirée portes ouvertes

## Présents

Grégoire, Quentin, Félix, Maïeul

## Rappel but de la soirée

- Faire découvrir l'astronomie à un public plus large
- Permettre aux stagiares de partager leurs connaissances acquises durant le séjour
- Permettre aux différents parcours d'interagir plus que dans le reste de la semaine
- Dans le cadre de la campagne 2019, nuit des étoiles décentralisée

## Conséquences du but

1. Il faut que ce soit bien organisé pour que le public se sente accueilli
2. Il faut que ce soit souple, libre et participatif pour le bien être des participant·e·s

## Comment faire participer les participant·e·s aux propositions

- L'indiquer dans le livret
- Mettre un tableau où l'on peut mettre les propositions
- Chaque responsable de parcours le rappelle en début de parcours, et peut, selon son parcours, fixer des objectifs pour cette soirée
- On met quelques exemples dans le tableau pour amorcer et on complète avec les idées si ça manque
- Les participants s'inscrivent dans les ateliers qu'ils souhaitent faire avec en général 3-4 personnes par atelier pour faire des roulements

## Horaire de la soirée / modalité pratique

A partir de 20h30, même s'il fait pas encore nuit. Un premier accueil, quelques gens restent pour la vaisselle.

## Propositions d'ateliers

Reporté dans le fichier proposition d'atelier, à part, qu'on conserve d'une année sur l'autre


## Matos

-  De quoi fabriquer un système solaire à l'échelle
- Télescope à moitié ouvert pour montrer la structure optique / téléscope peda
- De quoi faire une boîte à dons :)
- Fléchage porte ouverte des années précédentes
- Com AFA nuit des étoiles

## Communication

- Imprimer des affiches (différent formats?)
- Les coller en équipe dés le samedi
- https://www.google.com/maps/search/camping/@44.2755981,5.6827835,12.25z/data=!4m8!2m7!3m6!1scamping!2s05300+Barret-sur-Méouge!3s0x12cafff933a9d53d:0x40819a5fd97aa20!4m2!1d5.732606!2d44.262126
- Noter les spots pour l'année suivante
- Téléphone astro
- 2019 c'est nuit des étoiles, pas porte ouverte > est qu'on reprend la com AFA
- Parcours à faire le samedi pour toucher un max du publics (camping, office de tourisme, ...)
- contacts

## Répartition des tâches

- Maïeul : modifier le livre et soumettre à Félix, faire les documents internes
- Félix : chercher des contacts, les mettre dans le framagit, faire la com deux semaines
- JB / Dimitri : modifier les horaires sur le site de l'AFA
- Quentin : faire un plan gmaps pour le collage d'affiche



