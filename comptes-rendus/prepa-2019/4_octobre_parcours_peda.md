Présents : Maïeul, JB, Dimitri
# Préalable
La réunion portait sur les parcours péda. Mais on a confimé qu'on retient les parcours 1,2,3 Etoiles en observationnel.

# Parcours péda retenus

    - Ourses (Petite et Grande), cf. plus bas.

    - ALU (Niveau 1 et 2, cf. plus bas.)

    - Plané (Niveau 1 et 2, cf. plus bas.)

    - On ne met pas AVT, car on veut se concentrer à remplir les 3 parcours péda susmentionnés et à bien les préparer à amont.


# Contenu des parcours

Pour chaque parcours péda, il y aurait 2 niveaux:
    - Un niveau 1, plus guidé
    - Un niveau 2, en autonomie, plus experimentale

Pour accéder au niveau 2, il faut avoir le niveau 1.

Concrètement, on n'ouvrirait le niveau 2 qu'en 2020, sauf pour Ourse.

Et cela donnerait:

        - ALU :

    - Niveau 1 : on découvre le cadre complet + un module ALU à partir duquel on met en oeuvre une expérience réplicable avec les élèves, guidé par les formateur/trices. Chaque année, on choisit une thématique différente

    - Niveau 2 : en autonomie, les stagiaires découvrent des outils ALU et inventent des experiences en lien.

    - Ourse

    - Niveau 1 (Petite Ourse) : découvrir l'astro + le faire découvrir à des jeunes de 8-14 ans

    - Niveau 2 (Grande Ourse) : comme en Etoile 2, approfondir en autonomie une thématique et voir comment la transmettre. Reprendre les défis d'Etoile 2 comme base pour approfondir un projet.

    - Plané

    - Niveau 1 : découvrir le fonctionnement du plané, faire une animation à partir d'un modèle deja prêt

    - Niveau 2 : concevoir une séquence d'animation sur une thématique en planétarium mettant en œuvre une démarche experimentale + maitriser plus fortement l'outil en sachant le programmer etc.

## Précisions sur ALU
La formation comprend ceci:
    - découvrir les outils concernés, mais pas tous, en focalisant sur une thématique (par ex. Etoiles et Lumières)
    - comprendre la science qu'expliquent ces outils là
mais pas toute la théorie astro
    - mettre en pratique expérimentale à partir de ces outils là

L'idée n'est pas de faire découvrir tout ALU, mais la logique pédagogique qu'il y a derrière.

# Prérequis
- ALU :

    - Niveau 1 : Petite Ourse ou Etoile 1

    - Niveau 2 : ALU 1

- Petite Ourse : aucun
- Grande Ourse : petite ourse / Etoile 1 + experience d'animation
- Plané :

    - Niveau 1 : Petite Ourse / Etoile 1

    - Niveau 2 : Plané 1


# Formulaires  d'inscription :
    - Mettre une case à cocher pour les prérequis
    - Changer les questions pour le parcours ALU

# Nombre d'inscriptions
10 personnes par groupes, 12 formateur·trice·s, 4 cuistot·e·s, 1 coordinateur, 1 salarié, quelques parcours libre > 78 personnes.

# Taches
- JB/Dimitri trouver les formateur·trice·s d'ici décembre / janvier
- Maïeul : adapter les descriptions des parcours de  formation
- Maïeul : adapter le formulaire d'inscription

