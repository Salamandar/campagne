Présent : Maïeul, Félix, Sébastien, Quentin, Camille, Isabelle, Hugo, Dimitri

# Dates
Entre ces 3 possibilités,
- 29 juillet - 4 août > la préférence des responsables de parcours (en raison de la lune)
- 5 au 11 août > si pas possible la semaine précédente
- 24 au 31 août > clairement pas souhaitable

Ceci après sondage des responsables de parcours, à avoir avec l'équipe cuisine.

# Emploi du temps
On modifie la structure de l'après-midi, en gardant les mêmes durée pour chaque activité
1. Vie quotidienne
2. Ateliers de formation
3. Temps libre
4. Conférence
5. Apero+meteo
6. Repas

But : permettre aux gens de souffler, éviter des débordements de temps
Remarque : Maïeul à voir s'il peut étendre l'atelier de 1h

#Nombre de conférences
A rediscuter en mars de la pertinence de réduire à 2, en fonction de proposition de conférence.

# Parcours
## Parcours péda
La question du rythme du matin ou pas n'a pas été tranché, à voir avec les futur·e·s responsables de parcours

## Parcours ALU
Voir comment mieux combiner démarche experimentale et pédagogique.  Globalement il faut une discussion sur ce parcours

## Parcours intermédiaire
Pas de parcours entre étoile 1 et 2 mais pour l'étoile 2 :
	- mieux sonder les compétences en amont lors de la préparation de la fiche d'inscription
	- mieux annoncer les prérequis

Globalement les prérequis pourraient être mieux mis en valeur sur le site.

## Enfants sur la campagne
Globalement, réticence à l'idée de faire un parcours officiellement pour les enfants (y compris si le parcours a un vrai contenu et n'est pas une garderie).

On reste sur le cas par cas.
#Vie quotidienne
## Temps de ménage
Est-ce qu'on peut le réduire? Question à poser pendant la réunion vie quot

## Vaisselle
Elle est un peu longue. Prévoir 45 mn officiellement + modifier le livret pour donner des astuces de gain de temps?

# Équipe
Il faut vraiment 2 personnes par équipe, il faut que les gens ne paient pas leurs campagnes
- Etoile 1 : Camille + Quentin. Camille sous reserve de ne pas payer (ou participation raisonnable). Voir éventuellement de quelle manière Valentin peut s'impliquer.
- Etoile 2 : Isabelle oui mais ne sait pas encore ce qu'elle fera à partir de février > relancer Lionel + Lisa (Maïeul)?
- Etoile 3 : Hugo + Théo (?). Hugo contact Theo
- Matos: Sébastien (oui) + Frédéric (à confirmer)
- Parcours péda : à voir
- Vie quotidienne / animation
		- Lydie (accueil des gens?)
		- Félix (soirée porte ouverte)
		- Julien : informatique
		- Grégoire : vie quotidienne
		- Noëlie : animations + gestion de la parole

# Prochaines réunions
1. Vie quotidienne 4 octobre
2. Matériel (Frédéric + Séb) > a fixer
3. Parcours ALU (contenu, horaires, formateur) et plus globalement parcours péda (avec JB)
4. JB + Quentin : budget

#Compte framagit
https://framagit.org/astroplasci/campagne
Sur le compte sont versionnés (en LaTeX):
	- le livret
	- le trombi
	- les affichettes
	- les elements de parcours en .tex (etoile 2)

Me demander un accès si intéressé.
Dans un second temps, je vais réflechir à un aménagement du wiki pour les documents qui ne peuvent être versionnés (Word, Images, etc.)

