# Parcours de formation
- Nous n'acceptons pas de personne hors parcours de formation (sauf
nourrisons) : 2 pour, 2 pour sous réserve
- Nous n'acceptons pas de personne hors parcours de formation, mais une
personne peut venir un soir ou l'autre voir quelqu'un et manger avec
nous 4 pour => ADOPTÉ
- Nous acceptons des personnes hors parcours de formation > 1 contre, 1
pour sous réserve

Donc **"Nous n'acceptons pas de personne hors parcours de formation, mais
une personne peut venir un soir ou l'autre voir quelqu'un et manger avec
nous"**

Edito : en avril 2019, en raison de la situation financière, nous acceptons cela au cas par cas.

# Concernant les mineurs.

Proposition du bureau adoptée par consensus tacite.

- pas de mineur·e de moins de 15 ans, sauf exception (type : bénévole
impliqué·e qui vient avec son enfant)
- mineur·e de plus de 15 ans (donc a priori a même de suivre les
parcours)
        - ok si avec un responsable légal ou émancipé
        - si pas le cas:
                - une personne parraine le/la mineur·e
                - autorisation écrite signée et envoyé à Ris
