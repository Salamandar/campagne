# Rencontre avec l'écoloc (skype)

Maïeul + Amélie pour l'Ecoloc

## Nouvelles de l'écoloc en terme d'aménagement

- Une chambre handicapé avec salle de bain aménagé pour 2 personnes. Correspond à l'ancienne salle pour étoile 2.
- En juillet, il y aura 79 lits simples + 4 lit double = 87 places
- Des salles repeintes
- Propreté meilleure
- Mobilier toujours ancien
- 4 Armoires en plus dans la salle "picasso" (Etoile 1, la salle en longueur)
- Parking terrassé, et, a priori, sera marqué au sol au mois de juillet
- Pour l'instant, pas de chambre froide dans la grande cuisine (problème financier)

### Conséquences pour nous

On perd une salle de travail par rapport à l'an dernier. Il faut donc trouver une autre orga.
Pistes :

- petit ref, mais problème de partage de l'espace
- grand ref, mais idem
- salle bricolage (derrière le parking), mais elle est pas hyper glop en terme de confort
- sous la tonnelle

La question d'un espace détente se pose. Où pourrait-on mettre?

## arrivée la veille de la campagne

Aucun problème pour 15 personnes, voire plus si nécessaire

## Poubelle et signalitique

Achat de contenant plus imposant pour la cuisine et pour transporter jusqu'au composteur composter sera refait

## Problème de la fête de l'écoloc

Le samedi soir, il y a la fête de l'ecoloc. Il faut donc voir comment on peut cohabiter en pratique. A priori, il y aura 400 personnes !
18h-19h spectacle familiale public, puis fête jusqu'à tard.

### Ce qui est garanti pour nous

- conserver jusqu'au bout la salle des fêtes / matos astro
- conserver les salles du batiment principal, pas d'accès aux étages

### Ce à quoi nous devons nous engager

- grand ref doit être propre et vide de matériel après la soirée porte ouverte. Sera utilisé comme repli stratégique en cas de mauvais temps. Mais du coup on aura pas à le laver le samedi

### Questions pratiques à résoudre

- où fait-on les retransmissions ?
- comment gère-t-on le buffet du samedi midi
- comment gère-t-on le repas du samedi soir > est-ce que collaboration ecoloc / équipe cuisine possible -> Amélie appelle Caro

### Questions résolues

- les bénévoles de l'écoloc pourront trouver où se loger

## Communication
- Envoi communication séjour de la part de l'ecoloc
- Annonce au mois de juillet dans l'infolettre de l'ecoloc de la nuit des étoiles
- Amélie va nous envoyer la liste des correspondant locaux
- Ecoloc va nous envoyer nouveau plan dès que possible
- Ecoloc va nous envoyer info sur tri selectif
- Ecoloc va nous envoyer photo du portail
