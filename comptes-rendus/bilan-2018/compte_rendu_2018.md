# Deuxième étoile 2018
L'objectif de ce parcours de formation était d'approfondir ses connaissances en astronomie en mettant en oeuvre un projet expérimental. Le programme de la semaine alternait entre ateliers pratiques, discussions théoriques et nuits d'observation. 

## Dimanche
Après la conférence d'Aurélien sur la cosmologie, nous avons commencé le parcours tout en douceur avec un jeu d'une dizaine de minutes pour bouger un peu. Nous avons ensuite fait un tour de table pour permettre à chaque astronours ·e __*__ de se présenter et d'exposer ses attentes vis-à-vis du parcours de formation.

> __*__ astronours, astronourse : nom donné par l'équipe cuisine aux participants et participantes de la campagne astro

Quelques projets ont ensuite été présentés aux astronours sur lesquels ils pouvaient se pencher pendant la semaine de campagne. L'objectif de ces projets était de mettre en oeuvre une démarche expérimentale en astronomie. Les projets proposés étaient conçus pour être réalisables en une semaine, et devaient concerner des objets observables pendant la campagne astro 2018.

Nous nous sommes répartis en trois groupes, travaillant chacun sur un projet différent :
- cartographier le ciel « à l'ancienne »
- mesurer la distance de la Terre à Mars
- cataloguer les étoiles en fonction de leur spectre

Un quatrième projet avait été présenté : celui de peser Jupiter. Mais à cause du relief autour du champ astro, Jupiter se couchait trop tôt pour être bien observée.

Certains projets étaient susceptibles de n'occuper qu'une partie de chaque nuit d'observation. Par exemple, le projet concernant Mars nécessite plusieurs clichés espacés de plusieurs nuits, mais ne requiert pas de prendre des mesures tout au long de la nuit. Par conséquent, pour combler d'éventuels temps morts lors des séances d'observations, plusieurs défis ont été proposés aux astronours. 
Ces défis étaient des activités de courte durée,  mais contrairement aux projets, aucune indication concernant leur faisabilité n'était donnée. Parmi ces défis étaient les suivants : 
- observer Uranus
- inventer une constellation
- décrire un chemin d'étoiles
- repérer une constellation issue d'une mythologie autre que la mythologie greco-romaine
- mesurer le passage du temps sans utiliser de montre
- ...

Nous pouvions librement en mener un ou plusieurs, en parallèle avec le projet.

L'après-midi s'est terminée avec un atelier d'assemblage et de mise en station des télescopes. Pendant cet atelier, nous avons pu prendre en main le matériel qui nous servirait aux observations du soir.

Un temps d'observation libre a été proposé pour la première nuit, afin de reprendre ses marques dans le ciel, de pointer quelques objets de Messier et s'entraîner à la mise en station.

## Lundi
Avant de nous réunir dans la salle du parcours 2^e^ étoile, nous avons pu assister à une conférence de Faustine sur les exoplanètes. 

Étant très nombreux lors de cette campagne astro (plus de 75 en tout), nous avons fait un nouveau tour de table des prénoms. Ce tour de table a aussi été l'occasion de faire le bilan de la nuit précédente, afin de déterminer les points à améliorer pour les nuits suivantes.

Nous nous sommes ensuite séparés en groupe de projet, afin de déterminer le protocole à mener pour réaliser le projet, ainsi que le matériel nécessaire pour le faire. Nous avons passé une demi-heure à faire des recherches et à réflechir aux modalités des projets.

Le reste de l'après-midi a été occupé par un atelier de collimation des télescopes de l'association, avec l'aide d'Aurélien et d'Héloïse.

Le début de la nuit était nuageux et ne permettait pas d'observer. Nous avons commencé par un atelier "lumière et spectres".

Après un bref topo théorique sur la lumière, des spectroscopes ont été distribués aux astronours, et la « chasse aux spectres » a été lancée. L'objectif était de comparer le spectre des différentes lumières de l'implantation. Des fentes et des réseaux ont ensuite été distribués pour construire des spectroscopes rudimentaires (encore bravo à Victorien pour son combo spectroscope + portable avec le scotch). Nous avons également sorti le LHIRES de sa malle pour permettre au groupe du projet de catalogue d'étoiles de comprendre son fonctionnement.

![Vue à travers un spectroscope](https://silica.io/wp-content/uploads/2018/08/IMAG0419.jpg)

## Mardi
Héloïse nous a proposé une conférence sur son sujet de recherche : la formation des systèmes planétaires. Les astronours du projet sur la spectroscopie nous ont ensuite proposé une présentation du fonctionnement du LHIRES.

Les observations pour les projets ont pu être entamées au cours de la nuit.

## Mercredi
Le mercredi était une journée d'activités libres, mais certains d'entre nous ont choisi d'en profiter pour avancer sur les projets au cours de la nuit.
Après le repas du soir différents groupes se sont formés entre murder party, chants et guitare, jeux de société,... avant une session d'observation du ciel!


## Jeudi
Après une conférence en plénière faite par Florian, nous avons observé le soleil. Nous avons pour cela pu utiliser un coronographe, une lunette avec un filtre solaire et un C8 avec un filtre solaire. Stéphane et Sébastien nous ont aidé dans l'utilisation du matériel. 

Nous avons vu une tache à la surface du Soleil, ce qui était une bonne surprise, puisque nous sommes dans une période de faible activité solaire. 

![](https://silica.io/wp-content/uploads/2018/08/soleil.jpg)

Le début de soirée fut malheureusement nuageux, ce qui empêcha le groupe Mars d'effectuer une prise de vue convenable avec Mars + les étoiles alentours.

Le groupe illustration du ciel a avancé sur une esquisse représentant la Voie Lactée. Tout à l'oeil nu, dans le noir!


## Vendredi
L'après-midi a été consacrée au traitement des données collectées les soirs précédents et à la préparation de la retransmission du samedi.

La soirée aura été consacrée aux portes ouvertes, moment important de la campagne où les astronours peuvent transmettre un peu de leur savoir à des curieux.

![Panneau réalisé par le groupe d'astrodessin](https://i.imgur.com/TXaCnWE.jpg)

## Perspectives
En attendant la campagne de l'année prochaine, un dernier défi a été donné aux astronours·es : celui de contribuer à un projet de sciences participatives en astronomie tel que FRIPON ou Zooniverse. 